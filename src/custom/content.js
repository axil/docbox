var fs = require('fs')

/**
 * This file exports the content of your website, as a bunch of concatenated
 * Markdown files. By doing this explicitly, you can control the order
 * of content without any level of abstraction.
 *
 * Using the brfs module, fs.readFileSync calls in this file are translated
 * into strings of those files' content before the file is delivered to a
 * browser: the content is read ahead-of-time and included in bundle.js.
 */
module.exports =
  fs.readFileSync('./content/README.md', 'utf8') + '\n' +
  fs.readFileSync('./content/access_requests.md', 'utf8') + '\n' +
  fs.readFileSync('./content/session.md', 'utf8') + '\n' +
  fs.readFileSync('./content/oauth2.md', 'utf8') + '\n' +
  fs.readFileSync('./content/namespaces.md', 'utf8') + '\n' +
  fs.readFileSync('./content/users.md', 'utf8') + '\n' +
  fs.readFileSync('./content/keys.md', 'utf8') + '\n' +
  fs.readFileSync('./content/todos.md', 'utf8') + '\n' +
  fs.readFileSync('./content/projects.md', 'utf8') + '\n' +
  fs.readFileSync('./content/commits.md', 'utf8') + '\n' +
  fs.readFileSync('./content/repositories.md', 'utf8') + '\n' +
  fs.readFileSync('./content/repository_files.md', 'utf8') + '\n' +
  fs.readFileSync('./content/licenses.md', 'utf8') + '\n' +
  fs.readFileSync('./content/tags.md', 'utf8') + '\n' +
  fs.readFileSync('./content/branches.md', 'utf8') + '\n' +
  fs.readFileSync('./content/issues.md', 'utf8') + '\n' +
  fs.readFileSync('./content/merge_requests.md', 'utf8') + '\n' +
  fs.readFileSync('./content/notes.md', 'utf8') + '\n' +
  fs.readFileSync('./content/award_emoji.md', 'utf8') + '\n' +
  fs.readFileSync('./content/labels.md', 'utf8') + '\n' +
  fs.readFileSync('./content/milestones.md', 'utf8') + '\n' +
  fs.readFileSync('./content/members.md', 'utf8') + '\n' +
  fs.readFileSync('./content/project_snippets.md', 'utf8') + '\n' +
  fs.readFileSync('./content/services.md', 'utf8') + '\n' +
  fs.readFileSync('./content/deploy_keys.md', 'utf8') + '\n' +
  fs.readFileSync('./content/deploy_key_multiple_projects.md', 'utf8') + '\n' +
  fs.readFileSync('./content/builds.md', 'utf8') + '\n' +
  fs.readFileSync('./content/build_triggers.md', 'utf8') + '\n' +
  fs.readFileSync('./content/build_variables.md', 'utf8') + '\n' +
  fs.readFileSync('./content/deployments.md', 'utf8') + '\n' +
  fs.readFileSync('./content/enviroments.md', 'utf8') + '\n' +
  fs.readFileSync('./content/pipelines.md', 'utf8') + '\n' +
  fs.readFileSync('./content/runners.md', 'utf8') + '\n' +
  fs.readFileSync('./content/groups.md', 'utf8') + '\n' +
  fs.readFileSync('./content/settings.md', 'utf8') + '\n' +
  fs.readFileSync('./content/broadcast_messages.md', 'utf8') + '\n' +
  fs.readFileSync('./content/sidekiq_metrics.md', 'utf8') + '\n' +
  fs.readFileSync('./content/system_hooks.md', 'utf8') + '\n' +
  fs.readFileSync('./content/ci/README.md', 'utf8') + '\n' +
  fs.readFileSync('./content/ci/builds.md', 'utf8') + '\n' +
  fs.readFileSync('./content/ci/runners.md', 'utf8') + '\n';

